from kivymd.uix.bottomnavigation import MDBottomNavigationItem

from Utility import utils

utils.load_kv("btnav_three.kv")


class BTNavThree(MDBottomNavigationItem):
    """
    Bottom Navigation Item Three.
    """
