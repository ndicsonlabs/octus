from kivy.properties import StringProperty
from kivymd.uix.list import IRightBodyTouch, OneLineListItem
from kivymd.uix.button import MDFlatButton
from kivymd.uix.boxlayout import MDBoxLayout



class DetailButton(IRightBodyTouch, MDBoxLayout):
    adaptive_width = True

class ListItemWithCheckbox(IRightBodyTouch, OneLineListItem):
    '''Custom list item.'''

    icon = StringProperty("android")


class RightCheckbox(IRightBodyTouch, MDFlatButton):
    '''Custom right container.'''
    text = StringProperty("Details")

