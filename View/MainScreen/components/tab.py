
from kivymd.uix.floatlayout import MDFloatLayout
from kivymd.uix.tab import MDTabsBase
from kivy.properties import StringProperty, ObjectProperty, ListProperty

class Tab(MDFloatLayout, MDTabsBase):
    '''Class implementing content for a tab.'''
    
    content = ObjectProperty(defaultvalue={})
    columns = ListProperty()
    rows = ListProperty()
    
    def on_enter(self):
        self.columns=self.content.get('column_data')
        self.rows=self.content.get('rows_data')
        print('this screen is tab')
