CREATE TABLE Offer (
	offer_id numeric,
	offer_name varchar,
	id integer PRIMARY KEY AUTOINCREMENT
);

CREATE TABLE User (
	service_no numeric,
	password varchar,
	email varchar,
	join_date datetime,
	id integer PRIMARY KEY AUTOINCREMENT
);

CREATE TABLE "Account Information" (
	account_type text,
	current_balance numeric,
	effective_time datetime,
	expiration_time datetime,
	relationship_type integer,
	associated_object_id integer,
	user integer,
	id integer PRIMARY KEY AUTOINCREMENT
);

CREATE TABLE "Product Information" (
	cycle_end_time datetime,
	cycle_start_time datetime,
	expiration_time datetime,
	effective_date datetime,
	rental_state varchar,
	offer_name varchar,
	user integer,
	id integer PRIMARY KEY AUTOINCREMENT
);

CREATE TABLE "Offer Details" (
	id integer PRIMARY KEY AUTOINCREMENT,
	offer numeric,
	price numeric,
	description text,
	validity integer
);

CREATE TABLE Validity (
	name varchar,
	duration numeric,
	unit varchar,
	id integer PRIMARY KEY AUTOINCREMENT
);







