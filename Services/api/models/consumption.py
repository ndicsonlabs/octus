# To use this code, make sure you
#
#     import json
#
# and then, to convert JSON from a string, do
#
#     result = consumption_from_dict(json.loads(json_string))

from dataclasses import dataclass
from typing import Any, List, TypeVar, Callable, Type, cast


T = TypeVar("T")


def from_str(x: Any) -> str:
    assert isinstance(x, str)
    return x


def from_list(f: Callable[[Any], T], x: Any) -> List[T]:
    assert isinstance(x, list)
    return [f(y) for y in x]


def to_class(c: Type[T], x: Any) -> dict:
    assert isinstance(x, c)
    return cast(Any, x).to_dict()


@dataclass
class AccountList:
    account_type: str
    associated_object_id: str
    current_balance: str
    effective_time: str
    expiration_time: str
    relationship_type: int

    @staticmethod
    def from_dict(obj: Any) -> 'AccountList':
        assert isinstance(obj, dict)
        account_type = from_str(obj.get("accountType"))
        associated_object_id = from_str(obj.get("associatedObjectId"))
        current_balance = from_str(obj.get("currentBalance"))
        effective_time = from_str(obj.get("effectiveTime"))
        expiration_time = from_str(obj.get("expirationTime"))
        relationship_type = int(from_str(obj.get("relationshipType")))
        return AccountList(account_type, associated_object_id, current_balance, effective_time, expiration_time, relationship_type)

    def to_dict(self) -> dict:
        result: dict = {}
        result["accountType"] = from_str(self.account_type)
        result["associatedObjectId"] = from_str(self.associated_object_id)
        result["currentBalance"] = from_str(self.current_balance)
        result["effectiveTime"] = from_str(self.effective_time)
        result["expirationTime"] = from_str(self.expiration_time)
        result["relationshipType"] = from_str(str(self.relationship_type))
        return result


@dataclass
class ProductList:
    cycle_end_time: str
    cycle_start_time: str
    effective_date: str
    expiration_time: str
    offer_name: str
    rental_state: str

    @staticmethod
    def from_dict(obj: Any) -> 'ProductList':
        assert isinstance(obj, dict)
        cycle_end_time = from_str(obj.get("cycleEndTime"))
        cycle_start_time = from_str(obj.get("cycleStartTime"))
        effective_date = from_str(obj.get("effectiveDate"))
        expiration_time = from_str(obj.get("expirationTime"))
        offer_name = from_str(obj.get("offerName"))
        rental_state = from_str(obj.get("rentalState"))
        return ProductList(cycle_end_time, cycle_start_time, effective_date, expiration_time, offer_name, rental_state)

    def to_dict(self) -> dict:
        result: dict = {}
        result["cycleEndTime"] = from_str(self.cycle_end_time)
        result["cycleStartTime"] = from_str(self.cycle_start_time)
        result["effectiveDate"] = from_str(self.effective_date)
        result["expirationTime"] = from_str(self.expiration_time)
        result["offerName"] = from_str(self.offer_name)
        result["rentalState"] = from_str(self.rental_state)
        return result


@dataclass
class Consumption:
    account_list: List[AccountList]
    fraud_state: str
    life_cycle_state: str
    product_list: List[ProductList]
    service_no: int

    @staticmethod
    def from_dict(obj: Any) -> 'Consumption':
        assert isinstance(obj, dict)
        account_list = from_list(AccountList.from_dict, obj.get("accountList"))
        fraud_state = from_str(obj.get("fraudState"))
        life_cycle_state = from_str(obj.get("lifeCycleState"))
        product_list = from_list(ProductList.from_dict, obj.get("productList"))
        service_no = int(from_str(obj.get("serviceNo")))
        return Consumption(account_list, fraud_state, life_cycle_state, product_list, service_no)

    def to_dict(self) -> dict:
        result: dict = {}
        result["accountList"] = from_list(lambda x: to_class(AccountList, x), self.account_list)
        result["fraudState"] = from_str(self.fraud_state)
        result["lifeCycleState"] = from_str(self.life_cycle_state)
        result["productList"] = from_list(lambda x: to_class(ProductList, x), self.product_list)
        result["serviceNo"] = from_str(str(self.service_no))
        return result


def consumption_from_dict(s: Any) -> Consumption:
    return Consumption.from_dict(s)


def consumption_to_dict(x: Consumption) -> Any:
    return to_class(Consumption, x)
