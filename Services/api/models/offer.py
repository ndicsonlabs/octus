#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  offer.py
#  
#  Copyright 2022 Cho Akum Ndi <ndicsonlabs@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
# To use this code, make sure you
#
#     import json
#
# and then, to convert JSON from a string, do
#
#     result = offer_from_dict(json.loads(json_string))

from enum import Enum
from dataclasses import dataclass
from typing import Any, List, TypeVar, Type, Callable, cast


T = TypeVar("T")
EnumT = TypeVar("EnumT", bound=Enum)


def from_str(x: Any) -> str:
    assert isinstance(x, str)
    return x


def from_int(x: Any) -> int:
    #assert isinstance(x, int) and not isinstance(x, bool)
    return x


def to_enum(c: Type[EnumT], x: Any) -> EnumT:
    assert isinstance(x, c)
    return x.value


def from_list(f: Callable[[Any], T], x: Any) -> List[T]:
    assert isinstance(x, list)
    return [f(y) for y in x]


def to_class(c: Type[T], x: Any) -> dict:
    assert isinstance(x, c)
    return cast(Any, x).to_dict()


class DisplayFlag(Enum):
    Y = "Y"


@dataclass
class Offer:
    display_flag: DisplayFlag
    key: str
    offer_id: int
    offer_name: str
    result_map: int

    @staticmethod
    def from_dict(obj: Any) -> 'Offer':
        assert isinstance(obj, dict)
        display_flag = DisplayFlag(obj.get("displayFlag"))
        key = from_str(obj.get("key"))
        offer_id = int(from_str(obj.get("offerId")))
        offer_name = from_str(obj.get("offerName"))
        result_map = from_int(obj.get("resultMap"))
        return Offer(display_flag, key, offer_id, offer_name, result_map)

    def to_dict(self) -> dict:
        result: dict = {}
        result["displayFlag"] = to_enum(DisplayFlag, self.display_flag)
        result["key"] = from_str(self.key)
        result["offerId"] = from_str(str(self.offer_id))
        result["offerName"] = from_str(self.offer_name)
        result["resultMap"] = from_int(self.result_map)
        return result


def offer_from_dict(s: Any) -> List[Offer]:
    return from_list(Offer.from_dict, s)


def offer_to_dict(x: List[Offer]) -> Any:
    return from_list(lambda x: to_class(Offer, x), x)

# """
# {
	# url:'http://154.72.188.25:80/web/offer-subscription!getAvailableOffersBtn.action',
	# type:'POST',
	# dataType:'json',
	# data:{'serviceNo':serviceNo,'key':key},
	# async:true,
	# success:function(data)
# }

# ################## Get Method ################

# offer_link = "http://154.72.188.25/web/offer-subscription!getAvailableOffers.action?serviceNo=620019966"
# querry_params = {"serviceNo": 620019966}
# ############ Post Method ###############
# subscribedoffer="http://154.72.188.25/web/offer-subscription!subscribedOffer.action?serviceNo=620019966&offerid=209616747&key=M6qZ7h8m6YIy//2AHkNW+3T5T52QZ5Wo6DzWfyMnfB7BGtSJIUXAousFN31A+dUJUCWomj1Pnl3lV7Oka2mvaLPLNy1SeahYz9mJOUfxDfg="
# subscribe_query={"serviceNo": 620019966,
# "offerid": 209616747,
# "key": "M6qZ7h8m6YIy//2AHkNW 3T5T52QZ5Wo6DzWfyMnfB7BGtSJIUXAousFN31A dUJUCWomj1Pnl3lV7Oka2mvaLPLNy1SeahYz9mJOUfxDfg="
	# }
# subscribed_offer_formData=dict(regbutton='Subscribe')	

# ######################################
# offer_details = r"http://154.72.188.25/web/offer-subscription!getOfferDetailDesc.action"
# offer_details_formData = {
# "id": 209616813,
# "name": "Toli Single L",
# "key": "0dT6cHpdGMukVGcDe0fnskACU+8Dt6o5Tpy3B/pwceZBnSF3ShWFKAHp8HtzoTiKZOeAFGFWiMR+QUeAZEOGLg=="
# "language": "en_US"}
# """
