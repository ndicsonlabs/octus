# To use this code, make sure you
#
#     import json
#
# and then, to convert JSON from a string, do
#
#     result = offer_detail_from_dict(json.loads(json_string))

from enum import Enum
from dataclasses import dataclass
from typing import Optional, Any, List, TypeVar, Type, cast, Callable


T = TypeVar("T")
EnumT = TypeVar("EnumT", bound=Enum)


def from_str(x: Any) -> str:
    assert isinstance(x, str)
    return x


def from_none(x: Any) -> Any:
    assert x is None
    return x


def from_union(fs, x):
    for f in fs:
        try:
            return f(x)
        except:
            pass
    assert False


def to_enum(c: Type[EnumT], x: Any) -> EnumT:
    assert isinstance(x, c)
    return x.value


def from_int(x: Any) -> int:
    assert isinstance(x, int) and not isinstance(x, bool)
    return x


def to_class(c: Type[T], x: Any) -> dict:
    assert isinstance(x, c)
    return cast(Any, x).to_dict()


def from_list(f: Callable[[Any], T], x: Any) -> List[T]:
    assert isinstance(x, list)
    return [f(y) for y in x]


class Validity(Enum):
    THE_24_HOURS = "24 hours"
    THE_30_DAYS = "30 days"
    THE_48_HOURS = "48 hours"
    THE_4_HOURS = "4 hours"
    THE_7_DAYS = "7 days"


@dataclass
class Details:
    description: str
    price: str
    airtime: Optional[str] = None
    sms: Optional[str] = None
    data: Optional[str] = None
    validity: Optional[Validity] = None

    @staticmethod
    def from_dict(obj: Any) -> 'Details':
        assert isinstance(obj, dict)
        description = from_str(obj.get("description"))
        price = from_str(obj.get("price"))
        airtime = from_union([from_str, from_none], obj.get("airtime"))
        sms = from_union([from_str, from_none], obj.get("sms"))
        data = from_union([from_str, from_none], obj.get("data"))
        validity = from_union([Validity, from_none], obj.get("validity"))
        return Details(description, price, airtime, sms, data, validity)

    def to_dict(self) -> dict:
        result: dict = {}
        result["description"] = from_str(self.description)
        result["price"] = from_str(self.price)
        result["airtime"] = from_union([from_str, from_none], self.airtime)
        result["sms"] = from_union([from_str, from_none], self.sms)
        result["data"] = from_union([from_str, from_none], self.data)
        result["validity"] = from_union([lambda x: to_enum(Validity, x), from_none], self.validity)
        return result


@dataclass
class OfferDetailElement:
    offer: int
    details: Details

    @staticmethod
    def from_dict(obj: Any) -> 'OfferDetailElement':
        assert isinstance(obj, dict)
        offer = from_int(obj.get("offer"))
        details = Details.from_dict(obj.get("details"))
        return OfferDetailElement(offer, details)

    def to_dict(self) -> dict:
        result: dict = {}
        result["offer"] = from_int(self.offer)
        result["details"] = to_class(Details, self.details)
        return result


def offer_detail_from_dict(s: Any) -> List[OfferDetailElement]:
    return from_list(OfferDetailElement.from_dict, s)


def offer_detail_to_dict(x: List[OfferDetailElement]) -> Any:
    return from_list(lambda x: to_class(OfferDetailElement, x), x)
