import time

def check(num,msg):
    if not num:
        while not num:
            print(f'Your {msg} can not be empty!')
            num = input(f'Please enter your {msg}:  ')
            
def subscribe_to_cool(serviceNo,password):
    from db.models import DataAccess
    from utils import get_subscribed_message, subscribed_status

    access = DataAccess(serviceNo=serviceNo,password=password)
    otp = input('Please enter the code:  ')
    check(otp,'Code')
    print('Attempting login')
    access.login(serviceNo, password, otp)

    if access.is_authenticated:
        print('Login Successful!')

    print('Accessing subscription offers')
    offers = access.get_offers_data()
    print('Data obtained and selecting an offer')
    offer=offers[11]

    print(f'The offer selected is: Offer Name "{offer.offer_name}"')
    ans = input("Enter [1 or 2]: 1) Yes or 2) No to accept/reject this offer >> ")
    if int(ans) == 1:
        r=access.subscribe(offer)
        print(r)
        return r
    else:
        print('Aborted!, exiting the system....')
        time.sleep(3)
        return 'Aborted!'

    
if __name__ == '__main__':
    num = input('Please enter your camtel serviceNo:  ')
    check(num,'CAMTEL Service number')    
    password = input('Please enter your password:  ')
    subscribe_to_cool(num,password)
