#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  errors.py
#  
#  Copyright 2022 Cho Akum Ndi <ndicsonlabs@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

#import Exception
from requests.exceptions import ConnectionError

class Error(Exception):
    """Base class for other exceptions

    Attributes:
        message -- explanation of the error
    """ 

    def __init__(self, message):
        self.message = message
        super().__init__(self.message)

class SystemAbnormalityError(Error):
    """Raised when an abnormality occurs during a connection to the system."""
    def __init__(self):
        super().__init__("System abnormalities,please try later!")

class BlockedAccountError(Error):
    """Raised when an account has been restricted to log in."""
    def __init__(self):
        super().__init__("Because of your improper operation, your account has been restricted to log in!")

class InvalidPasswordError(Error):
    """Raised when the Password is Invalid."""
    def __init__(self):
        super().__init__("Password is Invalid.")

class TooManyAttemptsError(Error):
    """Raised when an account has been temporarily restricted to log in due to too many login attempts."""
    def __init__(self):
        super().__init__("Your account is already locked,please try again later!")

class VerificationCodeError(Error):
    """Raised when the Verification code is Invalid."""
    def __init__(self):
        super().__init__("The verification code is error.")
        
class WrongNumberTypeError(Error):
    """Raised when a wrong account number is used to try to log in."""
    def __init__(self):
        super().__init__(" This system allows X-tremNet numbers to login only.")
