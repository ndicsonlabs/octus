import logging
from .config import APISERVICE_LOG_FILE
# Logging 
logging.basicConfig(
    handlers=[logging.FileHandler(filename=APISERVICE_LOG_FILE, encoding='utf-8', mode='a+')],
    format="%(asctime)s %(name)s:%(levelname)s:%(message)s",
    datefmt="%F %A %T",
    level=logging.DEBUG
    )
