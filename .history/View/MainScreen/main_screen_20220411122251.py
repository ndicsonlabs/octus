
from kivy.core.window import Animation
from kivy.utils import get_color_from_hex
from View.base_screen import BaseScreenView


class MainScreenView(BaseScreenView):
    """Implements the onboarding start screen in the user application."""
    def on_slide_complete(self, instance_carousel):
        print(self, instance_carousel,self.ids.box_dots.children)
        for instance_dot in self.ids.box_dots.children:
            if instance_dot.index == instance_carousel.index:
                a=Animation(
                    md_bg_color=get_color_from_hex("#00008B"), d=0.2
                )
                print(a)
                a.start(instance_dot)
            else:
                Animation(
                    md_bg_color=self.theme_cls.disabled_hint_text_color, d=0.2
                ).start(instance_dot)

    def model_is_changed(self):# -> NoReturn:
        """
        Called whenever any change has occurred in the data model.
        The view in this method tracks these changes and updates the UI
        according to these changes.
        """

        
