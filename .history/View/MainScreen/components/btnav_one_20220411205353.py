from kivymd.uix.bottomnavigation import MDBottomNavigationItem
from kivy.properties import StringProperty, ObjectProperty

from Utility import utils

utils.load_kv("btnav_one.kv")


class BTNavOne(MDBottomNavigationItem):
    """
    Bottom Navigation Item One.
    """
    available_bal = StringProperty()
    data_left = StringProperty()
    data_used = StringProperty()
    account_balances = ObjectProperty(defaultvalue={})
    your_subscriptions = ObjectProperty(defaultvalue={})
    
        
    def on_tab_switch(
        self, instance_tabs, instance_tab, instance_tab_label, tab_text
        ):
        '''Called when switching tabs.

        :type instance_tabs: <kivymd.uix.tab.MDTabs object>;
        :param instance_tab: <__main__.Tab object>;
        :param instance_tab_label: <kivymd.uix.tab.MDTabsLabel object>;
        :param tab_text: text or name icon of tab;
        '''
        
        #if tab_text == "Account Balances":
            #instance_tab.content = self.account_balances#tab_text
        #elif tab_text == "Your Subscriptions":
            #instance_tab.content = self.your_subscriptions#tab_text
        
