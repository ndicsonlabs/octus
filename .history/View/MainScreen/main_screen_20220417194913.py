from datetime import datetime as dt
from kivymd.icon_definitions import md_icons
from kivy.metrics import dp
from kivymd.uix.datatables import MDDataTable
from kivy.uix.anchorlayout import AnchorLayout  
from View.MainScreen.components.list_item import ListItemWithCheckbox
from View.MainScreen.components.tab import Tab
from View.MainScreen.components.list_item import DetailButton
from View.base_screen import BaseScreenView
from Services.api.models.consumption import Consumption
from Services.db.models import *

from Utility import utils

utils.load_kv("main_screen.kv")

class MainScreenView(BaseScreenView):
    """Implements the login start screen in the user application."""


    def on_enter(self):
        account_data = self.model.account_data
        product_data = self.model.product_data
        offer_details = self.model.get_offer_details_data()
        self.add_tab(
            account_list=account_data,
            product_list=account_data,
            )
        self.add_offers(offerData)
        
    def add_tab(self,account_list, product_list):
        date_converter = lambda d: dt.strptime(d,'%d/%m/%Y %H:%M:%S').strftime('%d/%m/%Y')
        data={
            
            "Your Subscriptions":dict(
            column_data=[
                ("Offer Name", dp(28)),
                ("Expires", dp(37)),
            ],
            row_data=[
                # (
                #     "COOL",
                #     ("alert", [255 / 256, 165 / 256, 0, 1], "04/04/2022"),
                # ),
                (
                    "Prepaid Balance" if product.offer_name=='PrepaidBalanceSubaccount' else product.offer_name,
                    ("alert-circle", [1, 0, 0, 1], str(date_converter(product.expiration_time))),
                )
            for product in product_list
            ]
            ), 
            
            "Account Balances":dict(
            column_data=[
                ("Type", dp(13)),
                ("Balance", dp(18)),
                ("Expires", dp(29)),
            ],
            row_data=[
                (
                    account.account_type,
                    account.current_balance,
                    ("alert", [255 / 256, 165 / 256, 0, 1], date_converter(account.expiration_time)),
                )
                for account in account_list
                # (
                #     "Bonus",
                #     "1080.00 FCFA",
                #     (
                #         "checkbox-marked-circle",
                #         [39 / 256, 174 / 256, 96 / 256, 1],
                #         "12/05/2022",
                #     )
                # ),
            ]), 
        }

        #self.ids.dashboard.account_balances=data["Account Balances"]
        #self.ids.dashboard.your_subscriptions=data["Your Subscriptions"]
        #data={"Account Balances":'Balance Information', "Your Subscriptions":'Subscription Information'}
        col=[
            ("Service No:", dp(10)),
            (str(consumption_data.service_no), dp(10)),
            ("Life Cycle State", dp(15)),
            (consumption_data.life_cycle_state, dp(10)),
            ("Fraud State", dp(30)),
            (consumption_data.fraud_state, dp(30)),
        ] 
        first_table=MDDataTable(column_data=col)        
        for title in ["Your Subscriptions", "Account Balances"]: 
            tab_content=self.make_table(data[title])
            tab=Tab(title=title)
            tab.add_widget(tab_content)
            self.ids.dashboard.ids.tabs.add_widget(tab)
            #print(self.ids.dashboard.ids.tabs.ids)
            
    def add_offers(self,offers_data):
        icons = list(md_icons.keys())
        for i in offers_data:
            self.ids.offer.ids.scroll.add_widget(
                ListItemWithCheckbox(text=f"{i.offer_name}")
            )  
            
            
    def make_table(self,data):
        return MDDataTable(
            column_data=data['column_data'],
            row_data=data['row_data']
            )
              
    
    def action_btn(self,ids,**kwarg):
        print('pressed',ids,kwarg)
        title = ids['title']
        print(title.lower()=="offer")
        self.ids.toolbar.title= title
    def model_is_changed(self):# -> NoReturn:
        """
        Called whenever any change has occurred in the data model.
        The view in this method tracks these changes and updates the UI
        according to these changes.
        """

        
