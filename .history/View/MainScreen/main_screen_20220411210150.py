

from View.base_screen import BaseScreenView


class MainScreenView(BaseScreenView):
    """Implements the login start screen in the user application."""


    def on_enter(self):
        self.add_tab()
        self.add_offers()
        
    def add_tab(self):
        data={
            
            "Your Subscriptions":dict(
            column_data=[
                ("Offer Name", dp(28)),
                ("Expires", dp(37)),
            ],
            row_data=[
                (
                    "COOL",
                    ("alert", [255 / 256, 165 / 256, 0, 1], "04/04/2022"),
                ),
                (
                    "Pack XTREM WELCOME",
                    ("alert-circle", [1, 0, 0, 1], "01/01/2037"),
                )
            ]), 
            
            "Account Balances":dict(
            column_data=[
                ("Type", dp(13)),
                ("Balance", dp(18)),
                ("Expires", dp(29)),
            ],
            row_data=[
                (
                    "Bonus",
                    "1080.00 FCFA",
                    ("alert", [255 / 256, 165 / 256, 0, 1], "12/05/2022"),
                ),
                (
                    "Bonus",
                    "1080.00 FCFA",
                    (
                        "checkbox-marked-circle",
                        [39 / 256, 174 / 256, 96 / 256, 1],
                        "12/05/2022",
                    )
                ),
            ]), 
        }

        #self.ids.dashboard.account_balances=data["Account Balances"]
        #self.ids.dashboard.your_subscriptions=data["Your Subscriptions"]
        #data={"Account Balances":'Balance Information', "Your Subscriptions":'Subscription Information'}
        col=[
            ("Service No:", dp(10)),
            ("620018996", dp(10)),
            ("Life Cycle State", dp(15)),
            ("Active", dp(10)),
            ("Fraud State", dp(30)),
            ("ot blacklisted", dp(30)),
        ] 
        first_table=MDDataTable(column_data=col)        
        for title in ["Your Subscriptions", "Account Balances"]: 
            tab_content=self.make_table(data[title])
            tab=Tab(title=title)
            tab.add_widget(tab_content)
            self.ids.dashboard.ids.tabs.add_widget(tab)
            #print(self.ids.dashboard.ids.tabs.ids)
            
    def add_offers(self):
        icons = list(md_icons.keys())
        for i in range(30):
            self.ids.offer.ids.scroll.add_widget(
                ListItemWithCheckbox(text=f"Item {i}")
            )  
            
            
    def make_table(self,data):
        return MDDataTable(
            column_data=data['column_data'],
            row_data=data['row_data']
            )
              
    
    def action_btn(self,ids,**kwarg):
        print('pressed',ids,kwarg)
        title = ids['title']
        print(title.lower()=="offer")
        self.ids.toolbar.title= title
    def model_is_changed(self):# -> NoReturn:
        """
        Called whenever any change has occurred in the data model.
        The view in this method tracks these changes and updates the UI
        according to these changes.
        """

        
