
from typing import NoReturn, Union
from kivy.clock import Clock
from kivymd.uix.dialog import MDDialog
from kivymd.uix.snackbar import Snackbar

from Utility.observer import Observer
from View.base_screen import BaseScreenView


class LoginScreenView(BaseScreenView):
    """Implements the login start screen in the user application."""


    def __init__(self, **kw):
        super().__init__(**kw)
        self.dialog = MDDialog()
        self.dialog.bind(on_dismiss=self.controller.reset_data_validation_status)
        self.model.add_observer(self)

    def show_dialog_wait(self) -> NoReturn:
        """Displays a wait dialog while the model is processing data."""

        self.dialog.auto_dismiss = False
        self.dialog.text = "Attempting login..."
        self.dialog.open()

    def show_toast(self, interval: Union[int, float]) -> NoReturn:
        Snackbar(
            text="Login successful!",
            snackbar_x="10dp",
            snackbar_y="10dp",
            size_hint_x=.8,
            bg_color=self.theme_cls.primary_color,
        ).open()
        self.manager_screens.current = 'Main screen'
        

    def model_is_changed(self) -> NoReturn:
        """
        Called whenever any change has occurred in the data model.
        The view in this method tracks these changes and updates the UI
        according to these changes.
        """

        if self.model.data_validation_status:
            self.dialog.dismiss()
            Clock.schedule_once(self.show_toast, 1)
            #self.manager_screens.current = 'Main screen'
        if self.model.data_validation_status is False:
            self.dialog.text = "Wrong data!"
            self.dialog.auto_dismiss = True
