# The screens dictionary contains the objects of the models and controllers
# of the screens of the application.

from Model.main_screen import MainScreenModel
from Model.onboarding_screen import OnBoardingScreenModel
from Model.login_screen import LoginScreenModel
from Controller.main_screen import MainScreenController
from Controller.onboarding_screen import OnBoardingScreenController
from Controller.login_screen import LoginScreenController

screens = {
    "OnBoarding screen": {
        "model": OnBoardingScreenModel,
        "controller": OnBoardingScreenController,
    },
    "Login screen": {
        "model": LoginScreenModel,
        "controller": LoginScreenController,
    },
    "Main screen": {
        "model": MainScreenModel,
        "controller": MainScreenController,
    },
}
