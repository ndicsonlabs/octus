
import importlib

import View.OnBoardingScreen.onboarding_screen

# We have to manually reload the view module in order to apply the
# changes made to the code on a subsequent hot reload.
# If you no longer need a hot reload, you can delete this instruction.
importlib.reload(View.OnBoardingScreen.onboarding_screen)


class OnBoardingScreenController:
    """
    The `OnBoardingScreenController` class represents a controller implementation.
    Coordinates work of the view with the model.
    The controller implements the strategy pattern. The controller connects to
    the view to control its actions.
    """

    def __init__(self, model):
        self.model = model  # Model.onboarding_screen.OnBoardingScreenModel
        self.view = View.OnBoardingScreen.onboarding_screen.OnBoardingScreenView(controller=self, model=self.model)

    def on_tap_button_login(self) -> None:
        """Called when the `LOGIN` button is pressed."""

    def set_user_data(self, key, value) -> None:
        """Called every time the user enters text into the text fields."""

    def get_view(self) -> View.OnBoardingScreen.onboarding_screen.OnBoardingScreenView:
        return self.view
