import datetime
from peewee import *
from Services.api.config import DATABASE_FILE

database = SqliteDatabase(DATABASE_FILE)


class BaseModel(Model):
    
    STATUS = (
        (0,'initial'),
        (1,'loaded'),
        )
    
    state = IntegerField(choices=STATUS, default=0)
    created_on = DateTimeField(default=datetime.datetime.now)
    modified_on = DateTimeField

    @property
    @classmethod
    def is_initialised(cls):
        return cls.state == 0
    
    @property
    @classmethod
    def has_data(cls):
        return cls.state == 1

    @classmethod
    def atomic_bulk_create(cls, model_list):
        with database.atomic():
            cls.bulk_create(model_list, batch_size=10)
    
    #@classmethod
    #def get_by_id(cls, model_id: int):
        #return cls.filter(cls.id == model_id).first()

    @classmethod
    def get_all(cls, skip: int = 0, limit: int = 100):
        return list(cls.select().offset(skip).limit(limit))
    
    def save(self, *args, **kwargs):
        self.modified = datetime.datetime.now()
        return super(BaseModel, self).save(*args, **kwargs)
    
    class Meta:
        database = database

##class DbState(BaseModel):

class Validity(BaseModel):
    duration = IntegerField()
    name = CharField()
    unit = CharField()

    class Meta:
        table_name = 'Validity'

class AccountInformation(BaseModel):
    account_type = CharField()
    associated_object_id = IntegerField()
    current_balance = CharField()
    effective_time = DateTimeField()
    expiration_time = DateTimeField()
    relationship_type = IntegerField()
    unit = CharField(null=True)
    
    @classmethod
    def get_all(cls, skip: int = 0, limit: int = 100):
        return list(cls.select().order_by(cls.expiration_time.desc()).offset(skip).limit(limit))
    #user = ForeignKeyField(User, backref='accountinformations')

    @classmethod
    def create_from_list(cls, account_list):
        accounts = [
            cls(
                account_type = account.account_type,
                associated_object_id = account.associated_object_id,
                current_balance = account.current_balance,
                effective_time = account.effective_time,
                expiration_time = account.expiration_time,
                relationship_type = account.relationship_type,
                state=1
                )
            for account in account_list
            ]

        cls.atomic_bulk_create(accounts)

    
    @classmethod
    def get_data_balance(cls):
        bal = cls.filter(
            cls.account_type=='DATA' | cls.account_type=='DATA_4G'
            ).order_by(cls.expiration_time.desc()).first()
        
        if bal:
            return bal.current_balance

        return 'NA'

    class Meta:
        table_name = 'Account Information'

class Offer(BaseModel):
    offer_id = IntegerField()
    offer_name = CharField()

    @classmethod
    def get_by_offer_id(cls,offer_id:int):
        return cls.filter(cls.offer_id==offer_id).first()

    @classmethod
    def create_from_list(cls, offer_list):
        offers = [
            cls(
                offer_id=offer.offer_id,
                offer_name=offer.offer_name
                )
            for offer in offer_list
            ]
        
        cls.atomic_bulk_create(offers)
        
    class Meta:
        table_name = 'Offer'

class OfferDetails(BaseModel):
    description = TextField()
    offer = ForeignKeyField(Offer, backref='offerdetails')
    price = DecimalField()
    validity = ForeignKeyField(Validity, backref='offerdetails', null=True, default=1)
    airtime = CharField(null=True)
    sms = CharField(null=True)
    data = CharField(null=True)

    # @classmethod
    # def create_from_dictlist(cls, offer_details_list):
    #     cls.atomic_bulk_create(offer_details_list)
        
    # @classmethod
    # def get_or_create_offer_details(cls, offer, offer_details):
        
    #     details, created = cls.get_or_create(
    #                  offer=offer,
    #                  )
    #     if created:
    #         validity=offer_details.get('validity',None).split()[0]
    #         description=offer_details.get('description')
    #         price=offer_details.get('price')
    #         if not not validity:
    #             details.validity = Validity.filter(duration=int(value)).first()
    #         details.description = description
    #         details.price = price
    #         details.save()
    #     return details

    class Meta:
        table_name = 'Offer Details'

class ProductInformation(BaseModel):
    cycle_end_time = DateTimeField()
    cycle_start_time = DateTimeField()
    effective_date = DateTimeField()
    expiration_time = DateTimeField()
    offer_name = CharField()
    rental_state = CharField()
    #user = ForeignKeyField(User, backref='productinformations')

    @classmethod
    def get_all(cls, skip: int = 0, limit: int = 100):
        return list(cls.select().order_by(cls.expiration_time.desc()).offset(skip).limit(limit))
        
    @classmethod
    def create_from_list(cls, product_list):
        products = [
            cls(
                cycle_end_time = product.cycle_end_time,
                cycle_start_time = product.cycle_start_time,
                effective_date = product.effective_date,
                expiration_time = product.expiration_time,
                offer_name = product.offer_name,
                rental_state = product.rental_state,
                state=1
                )
            for product in product_list
            ]

        cls.atomic_bulk_create(products)
	    
    class Meta:
        table_name = 'Product Information'

class User(BaseModel):
    email = CharField(null=True)
    join_date = DateTimeField(default=datetime.datetime.now)
    last_login = DateTimeField(null=True)
    login_count = IntegerField(default=0)
    password = CharField(null=True)
    service_no = CharField(unique=True)

    @classmethod
    def login_create_or_update_user(cls,service_no,password=None,email=None):
        # Simulate the user logging in. The login count and timestamp will be
        # either created or updated correctly.
        now = datetime.datetime.now()
        rowid = (cls
                 .insert(service_no=service_no,password=password,email=email, last_login=now, join_date=now, login_count=1)
                 .on_conflict(
                     conflict_target=(cls.service_no,),
                     preserve=[
                         cls.service_no,
                         cls.password,
                         cls.email,
                         cls.last_login,
                         cls.join_date,
                         cls.created_on,
                         ],  # Use the value we would have inserted.
                     update={cls.login_count: cls.login_count + 1,cls.state:1})
                 .execute())
        return rowid

    # @classmethod
    # def get_user(cls, user_id: int):
    #     return cls.filter(cls.id == user_id).first()

    @classmethod
    def get_user_by_service_no(cls, service_no: str):
        return cls.filter(cls.service_no == service_no).first()

    @classmethod
    def get_user_by_email(cls, email: str):
        return cls.filter(cls.email == email).first()


    class Meta:
        table_name = 'User'

		
# a dead simple one-to-many relationship: one user has 0..n messages, exposed by
# the foreign key.  because we didn't specify, a users messages will be accessible
# as a special attribute, User.message_set
class Message(BaseModel):
    user = ForeignKeyField(User, backref='messages')
    content = TextField()
    created_date = DateTimeField(default=datetime.datetime.now)
    is_read = BooleanField(default=True)

MODELS = [
            #DbState,
            User,
            ProductInformation,
            OfferDetails,
            Offer,
            AccountInformation,
            Validity,
            Message
        ]

@database.connection_context()
def prepare_database():
    # DB connection will be managed by the decorator, which opens
    # a connection, calls function, and closes upon returning.
    create_tables()  # Create schema.
    # if not BaseModel.is_initialised:
    #     load_initial_data()

def recreate_tables():
    database.drop_tables(MODELS)
    database.create_tables(MODELS)

def create_tables():
    with database:
        recreate_tables()

def is_init(m):
    for i in m:
        if not i:
            return False
    return True

DATABASE_STATUS = is_init([d.has_data for d in [Offer,OfferDetails,Validity]])
