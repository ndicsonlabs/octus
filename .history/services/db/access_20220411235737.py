import os
import logging
import multitasking
import time
import json
from Services.db.models import *
from Services.api.backend import api
from Services.api.errors import Error
from Services.api.config import JSON_FILE_DIR
from Services.api.models.consumption import consumption_from_dict,consumption_to_dict
from Services.api.models.offer import offer_from_dict, offer_to_dict
from Services.api.utils import get_subscribed_message, subscribed_status,get_details_as_dict,get_offer_description,save_json

multitasking.set_max_threads(10)

# Logging
#logging.basicConfig(filename='app.log', encoding='utf-8', level=logging.DEBUG)

# Constants
CREDIT_BAL = 'PrepaidBalanceSubaccount'


class DataAccess(object):
    serviceNo=None
    api=api
    

    def __init__(self, serviceNo, password=None):
        self.serviceNo=serviceNo
##        user, created = User.get_or_create(
##            username = serviceNo,
##            password = password
##            )
##        if created:
##            user.save()
    def login(self, serviceNo, password, otpCode):
        try:
            self.api.login(
                otp_code=otpCode,
                serviceNo=serviceNo,
                password=password
                )
            if self.api.is_authenticated:
                self.save_consumption_data(
                    self.get_consumption_data()
                )

        except Error as e:
            logging.error(e)

    def get_consumption_data(self):
            return consumption_from_dict(self.api.get_consumption_data())
            
    def get_offers_data(self):
            p = self.api.get_offers_data(self.serviceNo)
            return offer_from_dict(p)
        
    def get_offer_details(self,an_offer):
        return self.api.get_offerDetailsPage(
                serviceNo=self.serviceNo,
                offer=an_offer
                )

    def subscribe(self,an_offer):
        return subscribed_status(
                get_subscribed_message(
                    self.api.get_subscribedPage(
                        serviceNo=self.serviceNo,
                        offer=an_offer
                        )
                    )
                )

    @multitasking.task
    def get_and_save_details_data(self):
        """
        Get details data from the website to be saved into the database.
        This method is completely asynchronous. It does not return any value.
        """
        data=[]
        offers_in_db = Offer.get_all()
        if not offers_in_db:
            print('no offer found in database')
            return
        
        for offer in offers_in_db: 
            #time.sleep(1)
            description = get_offer_description(self.get_offer_details(offer))
            #time.sleep(1)
            details = get_details_as_dict(description)
            
            if offer.offer_name.strip().lower() == 'spot':
                print('spot reached')
                details={'price':'100.00 FCFA', 'data':'0.5 GB', 'validity': '4 hours','description':"100.00 FCFA	= 0.5 GB valid 4 hours"}
                data.append({
                'offer':offer.offer_id,
                'details':details
                })
                continue

            data.append({
                'offer':offer.offer_id,
                'details':details
                })
            
        # save_json(data, 'offerDetailsData')        
        print(data)
        
    def save_consumption_data(self, data):
        # save_json(consumption_to_dict(data), 'consumptionData')
        if not AccountInformation.has_data:
            AccountInformation.create_from_list(data.account_list)
        if not ProductInformation.has_data:
            ProductInformation.create_from_list(data.product_list)

    def save_offers_data(self,data):
        save_json(offer_to_dict(data), 'offersData')
        Offer.create_from_list(data)
        
            
    @property
    def is_authenticated(self):
            return api.is_authenticated
            
    # def get_balance_for(self,name):
    #         for acc in self.data.account_list:
    #                 if acc.account_type == name:
    #                         return get_current_balance(acc.current_balance)
            #return self.api.get_current_balance(
    
    # def get_data_bal(self):
    #         if self.is_authenticated:
    #                 return self.get_balance_for('DATA') or self.get_balance_for('DATA_4G')
            
    # def get_available_bal(self):
    #         if self.is_authenticated:
    #                 return self.get_balance_for('PrepaidBalanceSubaccount')
                    
##    def get_offers(self, payload):
##            return offer_from_dict(self.api.get_
    # def get_user(self):
    #     return User.get(User.username==self.serviceNo)

    # def get_credit_balance(self):
    #     bal = AccountInformation.select().where(AccountInformation.account_type==CREDIT_BAL)
    #     if bal:
    #         return bal[0].current_balance
    #     return 'NA'

    # def get_data_balance(self):
    #     return AccountInformation.get_data_balance()

@multitasking.task
def initialize_database():
    print(DATABASE_STATUS)
    if DATABASE_STATUS:
        print('no need to initialize')
        return
    prepare_database()
    validity_list = [
        Validity(
            duration = duration,
            name = name,
            unit = unit,
        )

        for duration, name, unit in zip(
            [30,7, 4,24,48],
            ['MONTHLY','WEEKLY']+['DAILY']*3,
            2*['days']+['hours']*3
            )
    ]
    Validity.atomic_bulk_create(validity_list)

    with open(os.path.join(JSON_FILE_DIR,'offersData.json')) as f:
        data = f.read()
        offers_data = offer_from_dict(json.loads(data))
        Offer.create_from_list(offers_data)

    with open(os.path.join(JSON_FILE_DIR,'offerDetailsData.json')) as f:
        data = f.read()
        offer_details_data = json.loads(data)
        list_of_offer_details = []
        for offer_details in offer_details_data:
            offer = Offer.get_by_offer_id(offer_details.get("offer"))
            details = offer_details.get("details")
            description = details.get('description')
            price = details.get('price')
            sms = details.get('sms')
            airtime = details.get('airtime')
            data = details.get('data')
            value = details.get('validity')
            if not not value:
                duration=int(value.split()[0].strip()) 
                validity = Validity.get(duration=duration)
            else:
                validity = None

            list_of_offer_details.append(
                OfferDetails(
                    offer=offer,
                    validity=validity,
                    sms=sms,
                    airtime=airtime,
                    data=data,
                    price=price,
                    description=description
                )
            )

        OfferDetails.atomic_bulk_create(list_of_offer_details)

    #gen_data = lambda duration, name, unit: dict(duration=duration, name=name, unit=unit)
