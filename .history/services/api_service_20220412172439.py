import time
import multitasking
import logging
from Services.api.backend import api
from Services.api.models.consumption import consumption_from_dict
from Services.api.models.offer import offer_from_dict
from Services.db.access import DataAccess
from Services.api.utils import get_subscribed_message, subscribed_status, get_current_balance
from Services.api.errors import Error
from random import sample, randint
from string import ascii_letters

multitasking.set_max_threads(5)

class ApiService:
    api = api
    consumption = None
    #is_authenticated = False
    consumption_data = None
    offers_data = None
    access = None
    def login(self, serviceNo, password, otpCode):
        self.access = DataAccess(serviceNo=serviceNo,password=password)
        print('Attempting login')
        self.access.login(serviceNo, password, otpCode)

        if self.access.is_authenticated:
            print('Login Successful!')
            # self.access.save_consumption_data(self.get_consumption_data())
            # print('Accessing subscription offers')
            # offers = self.access.get_offers_data()
            # self.access.save_offers_data(offers)
            return dict(status='Successful',message="Login Successful",errors=None)
        return dict(status='Unsuccessful',message="Login Unsuccessful",errors=True)

    def get_consumption_data(self):
        return consumption_from_dict(self.api.get_consumption_data())

    def get_offers_data(self):
        p = self.api.get_offers_data()
        return offer_from_dict(p)
            
    @property
    def is_authenticated(self):
        return self.api.is_authenticated
            
    # def get_balance_for(self,name):
    #     for acc in self.data.account_list:
    #         if acc.account_type == name:
    #             return get_current_balance(acc.current_balance)

    def subscribe_to_an_offer(self,offer):
        print('Data obtained and selecting an offer')
        #offer=offers[11]
        if self.access.is_authenticated:
            r=self.access.subscribe(offer)
            return r
        else:
            print('Aborted!, exiting the system....')
            time.sleep(3)
            return 'Aborted!'
            
    def get_data_from_db(self):
        return {'account':self.access.get_all_account(),'product':self.access.get_all_product()}

@multitasking.task
def start_service():
    return ApiService()
    
api =  start_service()