#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  backend.py
#
#  Copyright 2022 Cho Akum Ndi <ndicsonlabs@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

import os
import time
import logging
from requests import Session
from requests.exceptions import ConnectionError
from .errors import Error
from .config import *
from .utils import *

logging.info("Backend Service launched")

########## BACKEND SERVICES ###########

class Api(object):
	_endPoints       = getEndPoints()
	_error_msg       = ''
	homePageUrl      = _endPoints['home']
	imageUrl         = _endPoints['image']
	consumptionUrl   = _endPoints['consumption']
	loginUrl         = _endPoints['login']
	userInformation  = _endPoints['userInfo']
	offersUrl		 = _endPoints['offers']
	offerDetailsUrl	 = _endPoints['offerDetails']
	subscribedUrl	 = None
	session          = Session()
	png_dir          = IMAGE_DIR
	code_image_path  = IMAGE_FILE
	
	def __init__(self):
		self._establishConnectivity()
		
	def __new__(cls):
		if not hasattr(cls, 'instance'):
			cls.instance = super(Api, cls).__new__(cls)
		return cls.instance
		
	def reload(self):
		self._establishConnectivity()
		
	def _establishConnectivity(self):
		try:
			self.session.get(self.homePageUrl)
			time.sleep(2)
			image_stream = self.session.get(self.imageUrl).content
			time.sleep(1)
			if not os.path.exists(self.png_dir):
				os.makedirs(self.png_dir)

			save_to_file(self.code_image_path, image_stream)

		except ConnectionError:
			logging.error('unable to connect due to network issues, please try again later')
			raise
			
	def login(self, otp_code, serviceNo, password):
		
		payload = login_payload(code=otp_code, number=serviceNo, password=password)
		
		# Check authentification
		self.verify(password, payload)
				
		return
		
	def verify(self, password, payload):
		try:
			if not password:
				attempt = 0
				for paswrd in ATTEMPTS:
					payload[USER_PASSWORD]=paswrd
					attempt+=1
					self.authenticate(payload)
					logging.info(f'This is attempt no. {attempt}')
					if self.is_authenticated:
						logging.info("Login successful after {} attempts: pass {}".format(attempt,pas))
						break
				if attempt == len(ATTEMPTS) and not self.is_authenticated:
					logging.info("Unable to login with default password. Please contact the administrator")
			else:
				payload[USER_PASSWORD]=password
				logging.info('Attempting to login with credentials')
				result = self.authenticate(payload)
				logging.info('Login process complete')
			return result
		
		except InvalidPasswordError:
			logging.warning('Invalid password exception occurred')
			
		except TooManyAttemptsError:
			logging.error('Too many attempt exception occurred')
			raise
			
		except VerificationCodeError:
			logging.error('Verification Code exception occurred')
			raise
			
		except Error as e:
			logging.error(e)
			
		except ConnectionError:
			logging.error('unable to connect due to network issues, please try again later')
			raise
		
	def authenticate(self, credentials):		
		result = check_for_error(
			self.post(self.loginUrl, credentials)
			)
		time.sleep(3)
		self.get(self.homePageUrl)
		return result
		
	@property
	def is_authenticated(self):
		answer = self.check_if_authentic()
		return answer
		
	def check_if_authentic(self):
		
		logging.info('Checking if authenticated')
		
		welcome, _token = self.mock_get(self.homePageUrl)
		
		if not(not _token) and len(welcome) > 9:
			logging.info('authentication confirmed')
			return True
			
		logging.info('Re-authentication Required')
		return False
		
	def get_consumption_data(self):
		return get_data_as_dict(self.get_consumptionPage())
		
	def get_offers_data(self,serviceNo):
		return get_data_as_dict(self.get_offersPage(serviceNo))
	
	def get_offersPage(self,serviceNo):
		if self.is_authenticated:
			return self.get(
				self.offersUrl,
				params=dict(serviceNo=serviceNo)
			)
		else:
			raise Error("Your session has expired please login again")
			
	def get_offerDetailsPage(self, serviceNo, offer):
		""" 
			parameter `payload` is a dictionary
			object with keys ('id','key','name','language')
			e.g. {'id':id,'key':key,'name':name,'language':'en_US'}
		"""
		data = self.get_offers_data(serviceNo)
		offer_payload={}
		for anoffer in data:
			#print(offer.offer_id ,anoffer['offerId'],offer.offer_name.strip(), anoffer['offerName'].strip())
			if int(offer.offer_id) == int(anoffer['offerId'].strip()):
				print('equal selected')
				offer_payload={
					'id':anoffer['offerId'],
					'name':anoffer['offerName'],
					'key':anoffer['key'],
					'language':'en_US'
						}
				offer_payload
				break
		print(offer_payload,offer)
				
		if self.is_authenticated:
			page = self.post(
				self.offerDetailsUrl,
				data=offer_payload
			)
			# Set the subscribed url
			self.subscribedUrl = get_subscription_action(page)
			return page
		else:
			raise Error("Your session has expired please login again in other to acess the details")
				
	def get_subscribedPage(self, serviceNo, offer):
		""" 
			parameter `payload` is a dictionary
			object with singl key; e.g. {'regbutton': 'Subscribe'}
		"""
		offer_page = self.get_offerDetailsPage(serviceNo=serviceNo, offer=offer)

		# obtaining the subscription url
		self.subscribedUrl = get_subscription_action(offer_page)
		subscribed_offer_formData={'regbutton': 'Subscribe'}
		print('subscription url is: %s'%(self.subscribedUrl))
		if self.is_authenticated and (not not(self.subscribedUrl)):
			page = self.post(
				self.subscribedUrl,
				data=subscribed_offer_formData
			)
			return page
		else:
			raise Error("Your session has expired please login again in other to acess the details")
			
	#def subscribe_to_plan(self):
		
	###  Pages ####
	
	def get_consumptionPage(self):
		if self.is_authenticated :
			return self.get(self.consumptionUrl)
		
		else:
			raise Error("Your session has expired please login again in other to acess the consumption page")
				
	def get(self,url, params=None):
		if not params:
			return self.session.get(url)
		else:
			return self.session.get(url,params=params)
		
	def post(self, url, data):
		return self.session.post(url, data=data)
	
	def mock_get(self, url):
		new_session = self.session
		return check_for_error(new_session.get(url)),new_session.cookies.get_dict().get(TOKEN_KEY)


api = Api()
