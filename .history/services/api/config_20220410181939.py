import os
import string

##### CONFIGURATIONS #########
# DIRECTORIES
API_DIR = os.path.dirname(os.path.abspath(__file__))
SERVICES_DIR = os.path.dirname(API_DIR)
LIBS_DIR = os.path.dirname(SERVICES_DIR)
PROJ_DIR = os.path.dirname(LIBS_DIR)

# DATABASE
DATABASE_FILE=os.path.join(SERVICES_DIR,'db','octus.db')

# CONFIGURING LOG FILE DIRECTORY AND FILES PATH
LOG_DIR = os.path.join(SERVICES_DIR,'logs')
SERVICE_LOG_FILE = os.path.join(LOG_DIR, 'service.log')
APISERVICE_LOG_FILE = os.path.join(LOG_DIR, 'api_service.log')
DBSERVICE_LOG_FILE = os.path.join(LOG_DIR, 'db_service.log')
APP_LOG_FILE = os.path.join(LOG_DIR, 'app.log')

# CONFIGURING UI RELATED DIRECTORY AND FILES PATH
ASSETS_DIR = os.path.join(PROJ_DIR,'assets')
IMAGE_DIR = os.path.join(ASSETS_DIR,'images')
JSON_FILE_DIR = os.path.join(ASSETS_DIR,'data')
FONTS_DIR = os.path.join(ASSETS_DIR,'fonts')
IMAGE_FILE = os.path.join(IMAGE_DIR,'code.png')
LOGO_PATH     = os.path.join(IMAGE_DIR,"camtellogo.png")


# REGEX Patterns
SMS_PATTERN = '(?P<sms>[0-9]{1,6}[\s\-\w]*SMS)'
VALIDITY_PATTERN = '(?P<validity>[\.0-9]{1,2}\s*day|[\.0-9]{1,2}\s*hour[s]*)'
CALL_UNITS_PATTERN = '(?P<airtime>[0-9]{1,6}\s*min[utes]+)'
PRICE_PATTERN = '(?P<price>[0-9]{1,3}\s*[0-9]{1,4}\s*F\s*CFA)'
DATA_PATTERN = '(?P<data>[0-9]{1,6}\s*[MG][Bo]\/{0,1}\/{0,1}[\w]{0,3})'
COMBINED_PATTERN = VALIDITY_PATTERN+'|'+PRICE_PATTERN+'|'+DATA_PATTERN+\
                   '|'+CALL_UNITS_PATTERN+'|'+SMS_PATTERN

# Datetime string format
DATE_TIME_FORMAT = '%d/%m/%Y %H:%M:%S'

# Backend Api Constants

TOKEN_KEY = 'NmbyUserToken'

BASE_URL = "http://154.72.188.25"  # IP is more reliable than "http://myxtremnet.cm"

ENDPOINTS = {
    ################## Requires a "GET" Method ################
    "home": "/web/index.action",
    "image": "/web/index!checkcode.action",
    "userInfo": "/web/user!modifyInfo.action",
    "consumption": "/web/billing-query!accountBalance.action",
    "offers":"/web/offer-subscription!getAvailableOffers.action",
    ################## Requires a "POST" Method ################
    "login": "/web/user!gotowt.action",
    "passwordChange": "/web/user!modifyPassword.action",
    "offerDetails":"/web/offer-subscription!getOfferDetailDesc.action",
}

DIGITS = string.digits
TEN_DIGITS = DIGITS[1:]+DIGITS[0]
NINE_DIGITS = TEN_DIGITS[:-1]
EIGHT_DIGITS = NINE_DIGITS[:-1]
SIX_DIGITS = EIGHT_DIGITS[:-2]

ATTEMPTS = [
    EIGHT_DIGITS,
    SIX_DIGITS,
    NINE_DIGITS,
    TEN_DIGITS,
    DIGITS
    ]

USER_LOGIN_NAME = "userVO.loginName"
USER_PASSWORD =  "userVO.nmby"
CHECK_CODE = "checkCodes"

ID = "id"
NAME = "name"
KEY = "key"
LANGUAGE = "language"

LANGUAGE_CODE = "en_US"

DEVICE_WIDTH, DEVICE_HEIGHT  = (360, 640)
