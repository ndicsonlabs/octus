import re
import json,logging
from urllib.parse import urlencode
from bs4 import BeautifulSoup
from .config import * #(USER_LOGIN_NAME, USER_PASSWORD, CHECK_CODE, ENDPOINTS,BASE_URL)
from .errors import (InvalidPasswordError, TooManyAttemptsError,
                    VerificationCodeError, BlockedAccountError)


# Functions
pattern_compiler = lambda p: re.compile(p,re.IGNORECASE)

find_all_match = lambda text: re.findall(pattern_compiler(COMBINED_PATTERN),text)

map_function = lambda v: ''.join(v)

getEndPoints=lambda : {k:BASE_URL+ENDPOINTS[k] for k in ENDPOINTS}

encodeUrl = lambda url_base, querystring : "%s?%s" % (url_base, urlencode(querystring))

jsonify_data = lambda data: json.dumps(data,indent=4)

def get_file_path(name):
    p = os.path.join(JSON_FILE_DIR,name+'.json')

    if not os.path.exists(JSON_FILE_DIR):
        os.makedirs(JSON_FILE_DIR)
    return p
        
        
def get_details_extracted(details:str):
    return list(
        map(
            map_function,
            find_all_match(details)
            )
        )

def save_json(data,name:str):
    with open(get_file_path(name),'w') as f:
        f.write(jsonify_data(data))
    print('done jsonify')
    
def get_details_as_dict(details:str):
    data=dict(description=details)
    ptn=[CALL_UNITS_PATTERN,VALIDITY_PATTERN,SMS_PATTERN,PRICE_PATTERN,DATA_PATTERN]
    extract = [data.update(
        re.search(pattern_compiler(p),details).groupdict()) \
        for p in ptn \
        if not(not(re.search(pattern_compiler(p),details)))
        ]
    #print(data)
    return data

def login_payload(code,number,password):
    return {
        USER_LOGIN_NAME: number,
        USER_PASSWORD: password,
        CHECK_CODE: code
    }

def save_to_file(file_path, image_stream):
        with open(file_path, 'wb') as f:
            f.write(image_stream)
        return

def to_string(page):
    return page.text

def make_soup(page):
    if not(type(page) is str):
        page = to_string(page)
    return BeautifulSoup(page,'lxml')

def check_error_message(error):
    if error == "System abnormalities,please try later!":
        logging.warning(" System abnormalities,please try later!")
        raise SystemAbnormalityError

    if error == "Password is Invalid.":
        logging.warning("Invalid pass error occurred")
        raise InvalidPasswordError

    elif error == "Your account is already locked,please try again later!":
        raise TooManyAttemptsError

    elif error == "The verification code is error.":
        raise VerificationCodeError

    elif error == "Because of your improper operation, your account has been restricted to log in!":
        raise BlockedAccountError

def check_for_error(page):
    
    soup=make_soup(page)# if not to_string(page) else make_soup(to_string(page))
    g=soup.find_all(class_='m_left')
    if not(not g):
        if g[0].h5.text.strip().lower()=='message':
            print('abnormality error')
            check_error_message(g[0].td.text.strip()) 
    H5 = soup.h5
    
    error_message=soup.find(class_="f_red")

    if not error_message:
        logging.info("No error detected")
        if not(not H5):
            return H5.get_text().strip()

    else:
        error = error_message.get_text().strip()
        check_error_message(error)
        
##def get_subscription_key():
##    soup=make_soup(html)
##    sub_k=soup.find(class_="intro1_active_tab").input.attrs['value']
##    print(sub_k)
##    return soup
        
def get_subscription_action(page):
    soup=make_soup(to_string(page))
    sub_url=soup.find(id="formUpdate").attrs['action']
    return sub_url
  
def get_subscribed_message(page):
    soup=make_soup(to_string(page))
    message=soup.find(class_='m_left').td.get_text()
    return message

def subscribed_status(message):
    if message.strip() == "The balance of the account is not enough.":
        return dict(status='failed',message=message,error=True)
    else:
        logging.info('Subscribed sucssfully')
        return {status:'success','message':message,'error':False}
         
def get_offer_details_data(page):
    soup=make_soup(page)
    description=soup.textarea.get_text()
    offer_name, offer_id, offer_price = [inputs.attrs['value'] for inputs in soup.find(class_='s_table').find_all('input')]
    return offer_id, offer_name, offer_price, description
         
def get_offer_description(page):
    soup=make_soup(page)
    description=soup.textarea.get_text()
    return description

def get_data_as_dict(page)-> dict:
    consumption_data_pattern = re.compile(r'eval\((.+)\).+')
    extracted = re.search(consumption_data_pattern, to_string(page)).group(1)
    to_dict = json.loads(extracted)
    return to_dict

def get_current_balance(bal:str)->str:
    current_balance_pattern = re.compile(r'([0-9\.]+)(FCFA|GB|Go)')
    extracted = re.search(current_balance_pattern, bal)
    _bal = extracted.group(1).strip()
    unit = extracted.group(2).strip()
    exp_u = 1024**3
    if unit == 'FCFA':
        balance = round(float(_bal))
    else:
        balance = int(_bal)
        if balance > 0:
            balance = round(float(balance/exp_u),3)
    return f"{balance} {unit}"
