
"""
Script for managing hot reloading of the project.
For more details see the documentation page -

https://kivymd.readthedocs.io/en/latest/api/kivymd/tools/patterns/create_project/

To run the application in hot boot mode, execute the command in the console:
DEBUG=1 python main.py
"""

import importlib
import os,re

from kivy import Config
from kivy.uix.screenmanager import ScreenManager

from PIL import ImageGrab

# TODO: You may know an easier way to get the size of a computer display.
resolution = ImageGrab.grab().size

# Change the values of the application window size as you need.
Config.set("graphics", "height", resolution[1])
Config.set("graphics", "width", "360")

from kivy.core.window import Window

# Place the application window on the right side of the computer screen.
Window.top = 10
Window.left = resolution[0] - Window.width

# from kaki.app import App as HotReloaderApp 
# from kivymd.app import MDApp
# from Services.api_service import api
# from Services.db.access import initialize_database

KV_FOLDER = os.path.join(os.getcwd(), "View",)

# class Octus(MDApp, HotReloaderApp):
    
#     def __init__(self, **kwargs):
#         super().__init__(**kwargs)
#         self.base = api
#         initialize_database()

#     KV_FILES = [
#         os.path.join(
#             rootp,
#             i
#         )\
#             for rootp,d,fs in os.walk(KV_FOLDER)\
#              for i in fs if re.match(re.compile('.+\.kv$'),i)
#     ]
#     #print(KV_FILES)
# # Class to watch from *.py files
#     # You need to register the *.py files in libs/uix/baseclass/*.py
#     CLASSES = {
#         'octus': 'main.Octus', 
#         #'LoginScreen': 'libs.uix.baseclass.login_screen', 
#         #'HomeScreen': 'libs.uix.baseclass.home_screen',
#         # 'BackgroundWidget': 'libs.uix.components.background_widget', 
#         # 'BTNavOne': 'libs.uix.baseclass.btnav_one', 
#         # 'BTNavTwo': 'libs.uix.baseclass.btnav_two', 
#         # 'BTNavThree': 'libs.uix.baseclass.btnav_three',
#         'LoginScreenController': 'Model.login_screen',
#         'LoginScreenModel': 'Controller.login_screen',
#         'LoginScreenView': 'View.LoginScreen.login_screen'
#         }  # NOQA: F821

#     # Auto Reloader Path
#     AUTORELOADER_PATHS = [
#         (".", {"recursive": True}),
#     ]

#     def build_app(self) -> ScreenManager:
#         """
#         In this method, you don't need to change anything other than the
#         application theme.
#         # """

        # import View.screens

        # Window.soft_input_mode = "below_target"
        # self.title = "octus"

        # self.theme_cls.primary_palette = "DeepPurple"
        # self.theme_cls.primary_hue = "900"

        # self.theme_cls.accent_palette = "Teal"
        # self.theme_cls.accent_hue = "500"

        # self.theme_cls.theme_style = "Light"
        
        # self.manager_screens = ScreenManager()
        # Window.bind(on_key_down=self.on_keyboard_down)
        # importlib.reload(View.screens)
        # screens = View.screens.screens

        # for i, name_screen in enumerate(screens.keys()):
        #     model = screens[name_screen]["model"](self.base)
        #     controller = screens[name_screen]["controller"](model)
        #     view = controller.get_view()
        #     view.manager_screens = self.manager_screens
        #     view.name = name_screen
        #     self.manager_screens.add_widget(view)

        # return self.manager_screens

#     def on_keyboard_down(self, window, keyboard, keycode, text, modifiers) -> None:
#         """
#         The method handles keyboard events.

#         By default, a forced restart of an application is tied to the
#         `CTRL+R` key on Windows OS and `COMMAND+R` on Mac OS.
#         """

#         if "meta" in modifiers or "ctrl" in modifiers and text == "r":
#             self.rebuild()


# Octus().run()

# After you finish the project, remove the above code and uncomment the below
# code to test the application normally without hot reloading.

# """
# The entry point to the application.
# 
# The application uses the MVC template. Adhering to the principles of clean
# architecture means ensuring that your application is easy to test, maintain,
# and modernize.
# 
# You can read more about this template at the links below:
# 
# https://github.com/HeaTTheatR/LoginAppMVC
# https://en.wikipedia.org/wiki/Model–view–controller
# """

from typing import NoReturn

from kivy.uix.screenmanager import ScreenManager

from kivymd.app import MDApp

from View.screens import screens
from Services.api_service import api
from Services.db.access import initialize_database


class Octus(MDApp):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.base = api
        initialize_database()
        self.load_all_kv_files(KV_FOLDER)
        # This is the screen manager that will contain all the screens of your
        # application.
        self.manager_screens = ScreenManager()
        
    def build(self) -> ScreenManager:
        """
        Initializes the application; it will be called only once.
        If this method returns a widget (tree), it will be used as the root
        widget and added to the window.

        :return:
            None or a root :class:`~kivy.uix.widget.Widget` instance
            if no self.root exists.
        """

        self.theme_cls.primary_palette = "Amber"
        self.generate_application_screens()
        return self.manager_screens

    def generate_application_screens(self) -> NoReturn:
        """
        Creating and adding screens to the screen manager.
        You should not change this cycle unnecessarily. He is self-sufficient.

        If you need to add any screen, open the `View.screens.py` module and
        see how new screens are added according to the given application
        architecture.
        """

        for i, name_screen in enumerate(screens.keys()):
            model = screens[name_screen]["model"](self.base)
            controller = screens[name_screen]["controller"](model)
            view = controller.get_view()
            view.manager_screens = self.manager_screens
            view.name = name_screen
            self.manager_screens.add_widget(view)


Octus().run()
