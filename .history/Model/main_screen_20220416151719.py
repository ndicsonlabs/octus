import multitasking
from datetime import datetime
from typing import NoReturn

from Model.base_model import BaseScreenModel
from Services.db.models import *


multitasking.set_max_threads(10)

date_converter = lambda d: str(dt.strptime(d,'%d/%m/%Y %H:%M:%S').strftime('%d/%m/%Y'))


class MainScreenModel(BaseScreenModel):
    """Implements the logic of the user main screen."""

    available_balance:str
    data_used:str
    data_left:str
    total_data:str
    percent_usage:int
    expiration_time_elapsed:str


    def __init__(self, api):
        self.base = api
        self.load_data_from_db()
        # self.set_data()
        
    def set_available_balance(self,bal):
        self.available_balance=bal

    def set_expiration_time(self,date):
        self.expiration_time_elapsed = self.calculate_expiring(date)

    def set_pregress(self, data_left:int, total_data:int):
        return data_left/total_data * 100

    def make_table_data(self):
        account_table = dict(
            row_data=[
                # (
                #     "COOL",
                #     ("alert", [255 / 256, 165 / 256, 0, 1], "04/04/2022"),
                # ),
                (
                    "Prepaid Balance" if product.offer_name.strip()=='PrepaidBalanceSubaccount' else product.offer_name,
                    ("alert-circle", [1, 0, 0, 1], date_converter(product.expiration_time)),
                )
            for product in self.product_data
            ]
            ),
        # Data:
        #  {
        #      'login': 'User Login',
        #      'password': "12345",
        #  }
    #     self.user_data = {}
    #     self._data_validation_status = None
    #     self._observers = []

    # @property
    # def data_validation_status(self):
    #     return self._data_validation_status

    # @data_validation_status.setter
    # def data_validation_status(self, value):
    #     self._data_validation_status = value
    #     # We notify the View -
    #     # :class:`~View.LoginScreen.login_screen.LoginScreenView` about the
    #     # changes that have occurred in the data model.
    #     self.notify_observers()

    # @multitasking.task
    # def login(self):
    #     """
    #     Get data from the database and compares this data with the data entered
    #     by the user.
    #     This method is completely asynchronous. It does not return any value.
    #     """
    #     payload=[self.user_data[key] for key in self.user_data]
    #     print(payload)
    #     data = self.base.login(*payload)#.get_data_from_base_users()
    #     data_validation_status = False
        
    #     if data['status'] == 'Successful':
    #             data_validation_status = True
                
    #     self.data_validation_status = data_validation_status

    # def set_user_data(self, key, value):
    #     """Sets a dictionary of data that the user enters."""

    #     self.user_data[key] = value
    

    def notify_observers(self):
        """
        The method that will be called on the observer when the model changes.
        """

        for observer in self._observers:
            observer.model_is_changed()

    # def reset_data_validation_status(self):
    #     self.data_validation_status = None
    # 
    def load_data_from_db(self):
        data = self.get_data()
        self.account_data = data['account']
        self.product_data = data['product']
        self.offer_details_data =  data['offers']
        

    def calculate_expiring(self, date):
        now = datetime.now()
        
        if type(date) is not datetime:
            d=date_converter(date)

        if now <= date:
            return (date-now).days

    def get_consumption_data(self):
        return self.base.get_consumption_data()
        
    def get_data(self):
        return self.base.get_data_from_db()

    def add_observer(self, observer):
        self._observers.append(observer)

    def remove_observer(self, observer):
        self._observers.remove(observer)
