import multitasking
from datetime import datetime
from typing import NoReturn

from Model.base_model import BaseScreenModel
from Services.db.models import *


multitasking.set_max_threads(10)


class MainScreenModel(BaseScreenModel):
    """Implements the logic of the user main screen."""

    available_balance:str
    data_used:str
    data_left:str
    total_data:str
    percent_usage:int
    expiration_time_elapsed:str


    def __init__(self, api):
        self.base = api
        self.load_data_from_db()
        # Data:
        #  {
        #      'login': 'User Login',
        #      'password': "12345",
        #  }
    #     self.user_data = {}
    #     self._data_validation_status = None
    #     self._observers = []

    # @property
    # def data_validation_status(self):
    #     return self._data_validation_status

    # @data_validation_status.setter
    # def data_validation_status(self, value):
    #     self._data_validation_status = value
    #     # We notify the View -
    #     # :class:`~View.LoginScreen.login_screen.LoginScreenView` about the
    #     # changes that have occurred in the data model.
    #     self.notify_observers()

    # @multitasking.task
    # def login(self):
    #     """
    #     Get data from the database and compares this data with the data entered
    #     by the user.
    #     This method is completely asynchronous. It does not return any value.
    #     """
    #     payload=[self.user_data[key] for key in self.user_data]
    #     print(payload)
    #     data = self.base.login(*payload)#.get_data_from_base_users()
    #     data_validation_status = False
        
    #     if data['status'] == 'Successful':
    #             data_validation_status = True
                
    #     self.data_validation_status = data_validation_status

    # def set_user_data(self, key, value):
    #     """Sets a dictionary of data that the user enters."""

    #     self.user_data[key] = value
    

    def notify_observers(self):
        """
        The method that will be called on the observer when the model changes.
        """

        for observer in self._observers:
            observer.model_is_changed()

    # def reset_data_validation_status(self):
    #     self.data_validation_status = None
    # 
    def load_data_from_db(self):
        self.account_data = self.get_acount_data()
        self.product_data = self.get_product_data()
        self.offer_details_data = self.get_offer_details_data()

    def get_acount_data(self):
        return AccountInformation.get_all()
    
    def get_product_data(self):
        return ProductInformation.get_all()

    def get_offer_details_data(self):
        return OfferDetails.get_all()

    def get_consumption_data(self):
        return self.base.get_consumption_data()
        
    def get_data(self):
        return self.base.get_data_from_db()

    def add_observer(self, observer):
        self._observers.append(observer)

    def remove_observer(self, observer):
        self._observers.remove(observer)
